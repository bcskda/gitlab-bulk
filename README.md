# purble-pairs

Fetch numerous solutions from Gitlab MR-s and pass them through [MOSS](http://theory.stanford.edu/~aiken/moss/)

https://en.wikipedia.org/wiki/Purble_Place

Dependecies: see `requirements.txt`

For configuration edit this in code:

- `GROUP_ID` - group id at gitlab.com

- `TOKEN` - Access token with `api` scope, see [profile settings](https://gitlab.com/-/profile/personal_access_tokens)

- `MOSS_UID` - User id for MOSS, see [MOSS homepage](http://theory.stanford.edu/~aiken/moss/) for instructions

See `python3 purble-pairs.py --help`
