#!/usr/bin/env python3

import argparse
import os
import sys

import gitlab
import mosspy


GROUP_ID = None
TOKEN = "GITLAB_API_TOKEN"
MOSS_UID = ""  # http://theory.stanford.edu/~aiken/moss/

class SolutionFile:
    def __init__(self, proj, mr, task, path):
        self.project = proj
        self.mr = mr
        self.task = task
        self.path = os.path.join(task, path)

        self.branch = task
        if self.mr.state == 'merged':
            self.branch = 'master'  # TODO check default branch

        self.group_no = None
        for label in self.mr.labels:
            if label.isdigit():
                self.group_no = label
                break

    def download(self):
        file_content = self.project.files.get(
            file_path=self.path, ref=self.branch)
        local_dirname = os.path.join('.', self.path)
        local_filename = '{}-{}-{}'.format(
            self.group_no, self.project.name, os.path.basename(self.path))
        local_filename = os.path.join(local_dirname, local_filename)

        os.makedirs(local_dirname, exist_ok=True)
        with open(local_filename, "wb") as dest_stream:
            dest_stream.write(file_content.decode)


def download_all_projects(gl, group, task, path, extra_tags):
    tags = set(extra_tags).union([task])
    all_mrs = group.mergerequests.list(
        labels=[task]+extra_tags, all=True, as_list=False)
    failures = 0
    for mr in all_mrs:
        proj = gl.projects.get(mr.project_id)
        solution = SolutionFile(proj, mr, task, path)
        print('Trying {}'.format(solution.project.name))
        try:
            solution.download()
        except Exception as e:
            failures += 1
            mesg = 'Error, skipping {}: {}'.format(solution.project.name, e)
            print(mesg, file=sys.stderr)

    if failures == 0:
        print('Finished, OK')
    else:
        print('Failures:', failures)

def pass_through_moss(uid, solutions_dir):
    moss = mosspy.Moss(uid, "cc")
    for path in os.listdir(solutions_dir):
        moss.addFile(os.path.join(solutions_dir, path))
    report_url = moss.send()
    print('MOSS report url: {}'.format(report_url))
    print('Note: as of March 2021, report will expire in 2 weeks')

def cmdline(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--group", required=True,
                        help="`mipt/911` for specific group or `hse` for all groups in hse")
    parser.add_argument("--task", required=True, help="condvar/thread-pool")
    parser.add_argument("--file", required=True, help="tp/static_thread_pool.cpp")
    parser.add_argument("--moss", action='store_true', default=False, help="pass files through MOSS")
    parser.add_argument("--skip-download", action='store_true', default=False)
    return parser.parse_args()

def main():
    args = cmdline(sys.argv)
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=TOKEN)
    group = gl.groups.get(GROUP_ID)
    if not args.skip_download:
        download_all_projects(
            gl, group,
            args.task, args.file,
            extra_tags=args.group.split('/')
        )
    if args.moss:
        solutions_dir = os.path.join(args.task, args.file)
        pass_through_moss(MOSS_UID, solutions_dir)

if __name__ == '__main__':
    main()
